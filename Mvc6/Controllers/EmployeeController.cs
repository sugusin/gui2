﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc6.Models;
using log4net;

namespace Mvc6.Controllers
{
    public class EmployeeController : Controller
    {
        //
        // GET: /Employee/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            using(DBModel db = new DBModel())
            {
                List<Employee> empList = db.Employee.ToList<Employee>();
				List<Employee> empListFinal = new List<Employee>();

				foreach (Employee e in empList)
				{
					Employee eFinal = new Employee();
					eFinal.Age = e.Age;
					eFinal.EmployeeID = e.EmployeeID;
					eFinal.Name = e.Name;
					eFinal.Office = e.Office;
					eFinal.Position = e.Position;
					eFinal.Salary = e.Salary;
					eFinal.Date = e.Date.Value;
					empListFinal.Add(eFinal);	
			    }

				log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
				logger.Error("Error de sistema");
				
				return Json(new { data = empListFinal }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new Employee());
            else
            {
                using (DBModel db = new DBModel())
                {
                    return View(db.Employee.Where(x => x.EmployeeID == id).FirstOrDefault<Employee>());
                }
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(Employee emp)
        {
            using (DBModel db = new DBModel())
            {
                if (emp.EmployeeID == 0)
                {
                    db.Employee.Add(emp);
                    db.SaveChanges();
                    return Json(new { success = true, message = "Guardado con éxito" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    db.Entry(emp).State = System.Data.EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { success = true, message = "Actualizado con éxito" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (DBModel db = new DBModel())
            {
                Employee emp = db.Employee.Where(x => x.EmployeeID == id).FirstOrDefault<Employee>();
                db.Employee.Remove(emp);
                db.SaveChanges();
                return Json(new { success = true, message = "Borrado con éxito" }, JsonRequestBehavior.AllowGet);

            }
        }
    }
}
